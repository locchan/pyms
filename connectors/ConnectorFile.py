import io
import mmap
import os

from connectors.Connector import Connector


class ConnectorFile(Connector):

    def __init__(self, name, **kwargs):
        self.metric_storage = True
        self.config_storage = True
        self.metric_filepath = kwargs["METRIC_FILEPATH"]
        self.config_filepath = kwargs["CONFIG_FILEPATH"]
        super().__init__(self.metric_storage, self.config_storage, name, **kwargs)

    def validate_params(self, params):
        if "FILEPATH" in params:
            return True
        else:
            return False

    def connect_metrics(self):
        new = False
        if not os.path.exists(self.metric_filepath):
            new = True
        if not (self.metric_connection and isinstance(self.metric_connection, io.IOBase)):
            print("File connector: connecting to metrics file...")
            self.metric_connection = open(self.metric_filepath, "a")
        if new:
            self.metric_connection.write("# Metrics file generated by pyms #\n")
            self.metric_connection.flush()
        print("File connector: metrics connected.")
        return self.metric_connection

    def connect_config(self):
        new = False
        if not os.path.exists(self.config_filepath):
            new = True
        if not (self.config_connection and isinstance(self.config_connection, io.IOBase)):
            print("File connector: connecting to config file...")
            self.config_connection = open(self.config_filepath, "a")
        if new:
            self.config_connection.write("# Config file generated by pyms #\n")
            self.config_connection.flush()
        print("File connector: config connected.")
        return self.config_connection

    def send_metric_impl(self, metric_name, metric_value, timestamp):
        self.metric_connection.write(u'{} {} {}\n'.format(metric_name, metric_value, timestamp))
        self.metric_connection.flush()

    def check_duplicate(self, metric_name, metric_value, timestamp):
        return self.verify_metric_written(metric_name, metric_value, timestamp)

    def verify_metric_written(self, metric_name, metric_value, timestamp):
        metric_str = u'{} {} {}\n'.format(metric_name, metric_value, timestamp)
        with open(self.metric_filepath, 'rb', 0) as file, \
                mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ) as s:
            if s.find(bytearray(metric_str, "utf-8")) != -1:
                return True
            else:
                return False

    def is_in_config(self, parameter):
        with open(self.config_storage, 'r') as config_file:
            for number, line in enumerate(config_file):
                if line.split(" ")[0].strip() == parameter:
                    return number

    def send_config_impl(self, parameter, value):
        lineno = self.is_in_config(parameter)
        if lineno:
            with open(self.config_storage, "r+") as config_file:
                print(1)
