from connectors.Connector import Connector


class ConnectorMySQL(Connector):

    def __init__(self, name, **kwargs):
        self.connection = None
        super().__init__(self.metric_storage, self.config_storage, name, **kwargs)

    def validate_params(self, params):
        return True

    def connect(self):
        return True

    def send_metric_impl(self, metric_name, metric_value, timestamp):
        return True

    def check_duplicate(self, metric_name, metric_value, timestamp):
        return True

    def verify_metric_written(self, metric_name, metric_value, timestamp):
        return True