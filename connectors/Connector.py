class Connector:
    def __init__(self, metric_storage, config_storage, name="Dummy", **kwargs):
        self.name = name
        self.metric_connection = None
        self.config_connection = None
        self.metric_connected = False
        self.config_connected = False
        self.config_storage = config_storage
        self.metric_storage = metric_storage
        self._initialize(kwargs)
        print("Connector {} is initialized: metrics -> {}, config -> {}".format(self.name, self.metric_connected, self.config_connected))

    def _initialize(self, params):
        """
        Validates the input data for the connector by actually trying to connect.
        :param params: input parameters (basically, kwargs from __init__)
        :return: does not return anything. The result should be checked by *_connected and *_connection vars
        """
        self.validated = self.validate_params(params)
        if self.metric_storage:
            try:
                if self.connect_metrics():
                    print("Connector {} successfully connected to its metric storage".format(self.name))
                    self.metric_connected = True
                else:
                    print("Connector {} did not connect to its metric storage".format(self.name))
                    self.metric_connected = False
            except Exception as e:
                print("Connector {} caught an error while connecting to metric storage: {}".format(self.name, str(e)))
                self.metric_connected = False

        if self.config_storage:
            try:
                if self.connect_config():
                    print("Connector {} successfully connected to its config storage".format(self.name))
                    self.config_connected = True
                else:
                    print("Connector {} did not connect to its config storage".format(self.name))
                    self.config_connected = False
            except Exception as e:
                print("Connector {} caught an error while connecting to config storage: {}".format(self.name, str(e)))
                self.config_connected = False

    def validate_params(self, params):
        return True

    def connect_metrics(self):
        return True

    def connect_config(self):
        return True

    def send_metric(self, metric_name, metric_value, timestamp):
        self.connect_metrics()
        if self.check_duplicate(metric_name, metric_value, timestamp):
            print("Connector {} found a duplicate. Will not write this again".format(self.name))
            return True
        self.send_metric_impl(metric_name, metric_value, timestamp)
        if self.verify_metric_written(metric_name, metric_value, timestamp):
            return True
        else:
            print("Connector {} could not write a metric. You should retry this.".format(self.name))
            return False

    def send_config(self, parameter, value):
        self.send_config_impl(parameter, value)
        if self.verify_config_written(parameter, value):
            return True
        else:
            print("Connector {} could not write a config entry. You should retry this.".format(self.name))
            return False

    def send_config_impl(self, parameter, value):
        return True

    def check_duplicate(self, metric_name, metric_value, timestamp):
        return False

    def send_metric_impl(self, metric_name, metric_value, timestamp):
        print("Sent a metric: {} -> {}".format(metric_name, metric_value))

    def verify_metric_written(self, metric_name, metric_value, timestamp):
        return True

    def verify_config_written(self, parameter, value):
        return True
