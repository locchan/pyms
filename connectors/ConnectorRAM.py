import hashlib

from connectors.Connector import Connector


class ConnectorRAM(Connector):

    def __init__(self, name, **kwargs):
        self.metric_storage = True
        self.config_storage = False
        self.metrics = {}
        super().__init__(self.metric_storage, self.config_storage, name, **kwargs)

    def validate_params(self, params):
        return True

    def connect(self):
        return True

    def _calculate_md5(self, metric_name, metric_value, timestamp):
        return hashlib.md5(bytearray("{}{}{}".format(metric_name, metric_value, timestamp)))

    def send_metric_impl(self, metric_name, metric_value, timestamp):
        self.metrics[self._calculate_md5(metric_name, metric_value, timestamp)] = [metric_name, metric_value, timestamp]

    def check_duplicate(self, metric_name, metric_value, timestamp):
        return self._calculate_md5(metric_name, metric_value, timestamp) in self.metrics

    def verify_metric_written(self, metric_name, metric_value, timestamp):
        return self.check_duplicate(metric_name, metric_value, timestamp)
