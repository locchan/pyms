from connectors.ConnectorFile import ConnectorFile
from connectors.ConnectorRAM import ConnectorRAM

# Initializing all the connectors.
connector_file = ConnectorFile("File", METRIC_FILEPATH="/tmp/test.txt", CONFIG_FILEPATH="/tmp/test.cfg")
connector_ram = ConnectorRAM("RAM")

# The design is that if the primary connector is not available, the metrics will be written to secondary connectors
# After the connection to primary is restored, all data from secondary connectors will be transferred there

# The last resort is to write to the memory (fallback section) if no connectors are available
# Given the software is intended for Onion Omega2-type of hardware, this is REALLY the last resort, as there's not
# much ram there.

CONNECTORS = [
        [connector_file],  # Primary connectors
        [],                # Secondary connectors
        [connector_ram]    # Fallback connectors
     ]


def select_connector(metric=False, config=False):
    """
    Selects a connector in the priority order (primary -> secondary -> fallback)
    Will activate connections!
    :param metric: if you're looking for a metric connector
    :param config: if you're looking for a config connector
    :return: an object of a Connector-derived type.
    """
    if not metric and not config:
        return None
    good = False
    for connector_type in CONNECTORS:
        for connector in connector_type:
            if metric:
                connector.connect_metrics()
                good = connector.metric_connected
            if config:
                connector.connect_config()
                good = connector.config_connected
            if good:
                return connector
            else:
                continue

select_connector(config=True).send_config("test", 0)
